import numpy as np
#import matplotlib as mlp
import matplotlib.pyplot as plt
from scipy.io import loadmat



def load_raw_data(path):
    try:
        # hard code the key for convenience
        raw_data = loadmat(path)['amp_data']
    except IOError:
        print('IO Error: No such file [' + path + ']')
    else:
        return raw_data

def show_histogram(data):
    n, bins, patches = plt.hist(data,
            bins=300,
            density=False,
            facecolor='blue',
            edgecolor='black',
            alpha=0.7)
    plt.xlabel('amptitude')
    plt.ylabel('counts')
    plt.grid(True)
    plt.show()

def split_dataset(data):
    """Split data into train/test/validate sets.

        Args:
            data: np.narray, the whole dataset to be split
        Returns:
            (train, test, validation):
            train: (X_shuf_train, y_shuf_train),  training set, takes 70% of origin dataset
            validation: (X_shuf_val, y_shuf_val) validation set takes 15% of origin dataset
            test: (X_shuf_test, y_shuf_test) testing set, takes 15% of origin dataset
    """

    train_size = int(data.shape[0] * 0.7)
    validation_size = int(data.shape[0] * 0.15)
    train_data = data[:train_size]
    validation_data = data[train_size:train_size + validation_size]
    test_data = data[train_size + validation_size:]

    return ((np.array([ s[:20] for s in train_data ]), np.array([s[20:] for s in train_data])),
            (np.array([ s[:20] for s in validation_data ]), np.array([s[20:] for s in validation_data])),
            (np.array([ s[:20] for s in test_data ]), np.array([s[20:] for s in test_data])))


def load_data(path='./data.ln'):
    """Load data from dataset file, generate proper dataset for further process

    """

    # hard code here since there is no other data resource
    amp_data = np.array(load_raw_data(path)[1: -5]).reshape(-1, 21)

    # used to show histogram of origin data
    # show_histogram(amp_data)

    np.random.shuffle(amp_data)
    return split_dataset(amp_data)

def main():
    data = load_data()
    print('train_data', data[0], data[0][0].shape, data[0][1].shape)
    print('validation_data', data[1], data[1][0].shape, data[1][1].shape)
    print('test_data', data[2], data[2][0].shape, data[2][1].shape)

if __name__ == '__main__':
    main()





